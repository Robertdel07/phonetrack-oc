OC.L10N.register(
    "phonetrack",
    {
    "PhoneTrack" : "ติดตามสถานที่",
    "left" : "ซ้าย",
    "right" : "ขวา",
    "daily" : "ทุกวัน",
    "weekly" : "รายสัปดาห์",
    "PhoneTrack proximity alert (%s and %s)" : "ติดตามใกล้ชิดเตือน (%s และ %s)"
},
"nplurals=1; plural=0;");
